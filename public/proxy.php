<?php

$url = $_GET['site'];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$data = curl_exec($ch);
curl_close($ch);

echo $data;

////OLDER VERSION parsed on server side. Moved to client side.
// $content = file_get_contents($_GET["site"]);
//
// $content_temp = new DOMDocument();
// @$content_temp->loadHTML($content);
// $meta_video = null;
// // $meta_title = null;
//
// foreach ($content_temp->getElementsByTagName('meta') as $meta) {
//   if($meta->getAttribute('property')=='og:video') {
//     $meta_video = $meta->getAttribute('content');
//   }
//   // if($meta->getAttribute('property')=='og:title') {
//   //   $meta_title = $meta->getAttribute('content');
//   // }
// }
//
// echo $meta_video;
?>
