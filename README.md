https://oldhaunt.org/multyband

This is a little project I did to dip my toe in Vue. It turned out alright and it works! But it's obviously not built in an ideal way because I was working quickly and prioritizing learning Vue over making something truly scalable and maintainable.

Things that I can realistically do:

- Continue breaking app down into smaller components and functions
  - Move a lot of the raw HTML into components
  - Switch to JSX, add Babel
- Improve keyboard nav (while recognizing that iframes will always be a pain point)
- Shift albums up/down order
- Some layout options (e.g. toggle between large/small Bandcamp player)

Pipedreams:

- User accounts, login, save/manage your collections
- Drag-and-drop albums
