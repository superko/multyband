const path = require("path");

module.exports = {
  entry: "./src/index.js",
  output: {
    filename: "main.js",
    path: path.resolve(__dirname, "public/js")
  },
  mode: "production",
  resolve: {
    alias: {
      vue: "vue/dist/vue.common.js"
    }
  }
};
