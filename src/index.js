import Vue from "vue";
import VueResource from "vue-resource";
Vue.use(VueResource);

import { convertFromBase62, convertToBase62 } from "./helpers/base62.js";

var app = new Vue({
  el: "#app",
  data: {
    album: {
      albumId: "", // album/track ID number in Bandcamp
      type: "", // is it an album or a track
      href: "" // URL for this album/track's Bandcamp page
    },
    albums: [],

    // For the error message below the "Add" input
    error: {
      input: false,
      text: "Misc error"
    },

    // Bools to control show/hide of various UI elements
    show: {
      editIcon: false,
      edit: false,
      input: false,
      save: false,
      about: false,
      faq: false
    },

    io: {
      save: ""
    },

    rootURL: ""
  },

  mounted: function() {
    var currentURL = window.location.href;

    // On mount, check if there's load data ('?l=') in the URL; pass to loadData() if so
    if (currentURL.indexOf("?l=") != -1) {
      var startStr = currentURL.indexOf("?l=") + 3;
      var loadStr = currentURL.slice(startStr);
      this.loadData(loadStr);

      // Set rootURL to window URL without '?l=...' text
      this.rootURL = currentURL.substr(0, startStr - 3);
    } else {
      // If no load data, app will be blank, so show 'About' menu to start
      this.show.about = true;

      // Set rootURL to window URL
      this.rootURL = currentURL;
    }
  },

  methods: {
    addAlbum: function() {
      // Make sure there's an http(s) in front, and if not then add it
      if (
        this.album.href.indexOf("http://") === -1 &&
        this.album.href.indexOf("https://") === -1
      ) {
        this.album.href = "https://" + this.album.href;
      }

      // REMOVED: Bandcamp has custom domain feature so a Bandcamp page might not have bandcamp.com in the URL
      // Check if bandcamp.com is in the URL, if not then display an error
      // if (this.album.href.indexOf('bandcamp.com') === -1) {
      //   this.errorSwitch('nobandcamp');
      // } else {

      // Send URL to proxy.php, which will return the page's raw HTML content
      this.$http.get("./proxy.php", { params: { site: this.album.href } }).then(
        response => {
          var albumExists = true;

          // Parse the raw HTML; og:video meta tag contains the albumId we need
          var responseBody = response.body;
          var responseStart = responseBody.indexOf("og:video") + 8;
          var responseEnd = responseBody.indexOf('/">', responseStart);
          responseBody = responseBody.slice(responseStart, responseEnd);

          var startStr;

          // Determine from meta whether it's an album or track.
          if (responseBody.indexOf("album=") !== -1) {
            startStr = responseBody.indexOf("album=") + 6;
            this.album.type = "album";
          } else if (responseBody.indexOf("track=") !== -1) {
            startStr = responseBody.indexOf("track=") + 6;
            this.album.type = "track";
          } else {
            // If it is neither, then error is displayed
            albumExists = false;
            this.errorSwitch("badlink");
          }

          // If all is good, parse the og:video URL to isolate just the album/track ID number.
          if (albumExists) {
            var endStr = responseBody.indexOf("/", startStr);
            this.album.albumId = responseBody.substring(startStr, endStr);

            this.albums.push(this.album);
            this.errorSwitch("off");

            // 'Edit' menu option greyed out when there is no music, to avoid confusion. Show it once music has been added.
            if (this.show.editIcon === false) {
              this.show.editIcon = true;
            }
          }

          // Reset the album data object
          this.album = { albumId: "", type: "", href: "" };
        },
        response => {
          // If unable to access proxy.php, show error.
          this.errorSwitch("badconnection");
        }
      );
    },

    deleteAlbum: function(index) {
      if (confirm("Delete this item: are you sure?")) {
        this.albums.splice(index, 1);
        this.saveData();
      }

      // Hide 'Edit' menu option if there are no albums
      if (this.albums.length === 0) {
        this.show.editIcon = false;
      }
    },

    errorSwitch: function(type) {
      switch (type) {
        case "badlink":
          this.error.text =
            "Couldn't retrieve an album or song from this link. Please double check the address and try again.";
          this.error.input = true;
          break;
        case "badconnection":
          this.error.text =
            "Problem connecting to proxy or Bandcamp. Try again later?";
          this.error.input = true;
          break;
        case "badload":
          this.error.text = "Couldn't load savedata!";
          this.error.input = true;
          break;
        case "off":
          this.error.input = false;
          this.error.text = "Misc error";
          break;
        default:
          this.error.text = "Misc error";
          this.error.input = true;
          break;
      }
    },

    saveData: function() {
      // Save data is string of all albumIds (converted to base62), prefixed by 'a' or 't' (for album or track), separated by commas
      this.io.save = "";
      var saveArray = [];

      for (var i = 0; i < this.albums.length; i++) {
        var saveStr = "";
        if (this.albums[i].type === "album") {
          saveStr = "a";
        } else if (this.albums[i].type === "track") {
          saveStr = "t";
        } else {
          this.errorSwitch();
        }

        var albumIdBase62 = convertToBase62(this.albums[i].albumId);
        saveStr = saveStr + albumIdBase62;
        saveArray.push(saveStr);
      }

      if (saveArray.length) {
        // Save data added to the end of the rootURL to create the shareable link
        this.io.save = this.rootURL + "?l=" + saveArray.join(",");
      } else {
        // If there are no albums, user is prompted to add music
        this.io.save =
          'Click the "plus" icon to add some music, and then a link will be generated here.';
      }
    },

    loadData: function(loadStr) {
      // Load function parses string from URL after '?l=' and adds each album/track

      var data;

      if (loadStr.indexOf("%2C") != -1) {
        data = loadStr.split("%2C");
      } else {
        data = loadStr.split(",");
      }

      var i = 0;
      for (i = 0; i < data.length; i++) {
        console.log(data[i]);
        if (data[i].substr(0, 1) === "a") {
          this.album.type = "album";
        } else if (data[i].substr(0, 1) === "t") {
          this.album.type = "track";
        } else {
          this.errorSwitch("badload");
        }

        this.album.albumId = convertFromBase62(data[i].slice(1));
        this.albums.push(this.album);

        // Reset the album data object
        this.album = { albumId: "", type: "", href: "" };
      }

      // Show the edit icon, since there is music on the page.
      this.show.editIcon = true;
    }
  }
});

// Accessibility
document.addEventListener(
  "keydown",
  function(e) {
    if (e.keyCode == 27 || e.key == "Escape" || e.key == "Esc") {
      if (app.show.faq === true) {
        app.show.faq = false;
      } else {
        for (var item in app.show) {
          app.show[item] = false;
        }
      }
    }
    if (e.keyCode == 9 || e.key == "Tab" || e.key == "tab") {
      document.body.className = "accessibility-mode";
    }
  },
  true
);
