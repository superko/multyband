const base62 = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

export function convertToBase62(num) {
  var number = parseInt(num, 10);
  var letters = [];

  while (number > 0) {
    letters.unshift(base62[number % 62]);
    number = Math.floor(number / 62);
  }

  return letters.join("");
}

export function convertFromBase62(ltr) {
  var letters = ltr.split("");
  var number = 0;

  for (var i = 0; i < letters.length; i++) {
    number += base62.indexOf(letters[i]) * Math.pow(62, letters.length - 1 - i);
  }

  return number.toString();
}
